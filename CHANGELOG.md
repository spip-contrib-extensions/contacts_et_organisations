# Changelog

## Unreleased

### Fixed

- 2 notices PHP 8 (#15)
- Chaînes de langue distinctes pour nom/prénom des contacts et orgas

## 5.2.0 — 2023-03-14

### Added

- Compatible SPIP 4.2
- Compatible PHP 8.2

### Fixed

- Petites corrections pour éviter des deprecated
- Liaison d’auteurs sur les pages organisations ou contacts

## 5.x

## 4.x

## 3.0.0 — 2016-09-30

### Changed

- tous les liens sur spip_organisations_liens et spip_contacts_lien, utilisation uniquement de #FORMULAIRE_EDITER_LIENS

## 2.12.6 – 2015-12-16

### Added

- Ajout de squelettes de pagination alphabétique pour les contacts et les organisations.

## 2.8.0 — 2013-08-03

### Added

- Option pour supprimer les contacts/organisations si un auteur passe à la poubelle, et réciproquement.

## 2.7.0 — 2013-08-01

### Fixed

- Lors de la création d'un auteur depuis la fiche contact, passer par l'API d'édition pour que les pipelines soient appelés.

## 2.6.1 — 2013-02-09

### Fixed

- PhpDoc complet + sécurité PHP

## 2.6.2 — 2013-02-10

### Fixed

- Icones manquantes

## 2.6.1 — 2013-02-08

### Fixed

- Sur la page d'un auteur-contact, éviter un hit de préchargement qui finit en 403
- Afficher correctement le descripif des contacts ou organisation sur leur vue (coquille).
- Éviter une erreur js sur un auteur-contact lorque : les documents sont actifs
 sur les auteurs & contacts et qu'on souhaite ajouter un mot clé.

## 2.6.0 — 2013-02-03

### Fixed

- Critère {organisations_auteurs} équivalent à {contacts_auteurs} pour la table organisations,
 accompagné de sa balise #NOM_ORGANISATION

## 2.5.0 — 2013-02-02

### Fixed

- Nettoyages & gestion plus précise des jointures
- Squelettes de démo réécrits et complétés (?page=test/co)
- Critère {branche_organisation} équivalent à {branche} mais pour id_organisation
- Critère (ORGANISATIONS){compteur_contacts} et balise #COMPTEUR_CONTACTS
- Critère (AUTEURS){contacts_auteurs} et (CONTACTS){contacts_auteurs}
  pour créer la jointure entre ces tables et pouvoir attraper
  #NOM_CONTACT, #PRENOM_CONTACT, #CIVILITE_CONTACT depuis la table AUTEURS et
  #NOM_AUTEUR depuis la table CONTACTS
- Balise #LESORGANISATIONS comme #LESAUTEURS pour charger le modèle lesorganisations.
  On déprécie #ORGANISATIONS qui faisait cela.


## 2.2.0

### Fixed

— La colonne 'type_liaison' est limitée à 25 caractères pour avoir une primary key valide en SQLite et MySQL

## 2.0.0

### Added

— portage SPIP 3
- configurations
- liaisions via #FORMULAIRE_EDITER_LIENS et #FORMULAIRE_EDITER_LIENS_SIMPLES


## 1.3.9 — 2010-12-21

### Added

- ajout d'une proto-v-card avec feuille de style, appelée par le modele <auteur123>

## 1.3.7 — 2010-09-23

### Fixed

- suite remarque Aurélie sur le forum, changement du type de champ descriptif;


## 1.3.5 — 2010-08-24

### Added

- ajout picto auteur par défaut dans les listes;
- ajout picto "délier";

### Changed

- modif base 1.2.1 :
	- ajout champ organisations.secteur;
	- remplacement champ organisations.type par organisations.statut_juridique;
	- remplacement champ organisations.siret par organisations.identification;
