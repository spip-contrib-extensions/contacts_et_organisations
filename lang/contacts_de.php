<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/contacts?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'annuaire' => 'Verzeichnis',
	'annuaire_ajouter' => 'Verzeichnis hinzufügen',
	'annuaire_aucun' => 'Kein Verzeichnis',
	'annuaire_champ_descriptif_label' => 'Beschreibung',
	'annuaire_champ_identifiant_explication' => 'Ein eindeutiger Begriff, mit dem Inhalte ausgefiltert werden, z.B. {verzeichnis=vereine} für den Bezeichner "vereine".',
	'annuaire_champ_identifiant_label' => 'Bezeichner',
	'annuaire_champ_titre_label' => 'Titel',
	'annuaire_creer' => 'Neues Verzeichnis anlegen',
	'annuaire_creer_associer' => 'Neues Verzeichnis anlegen und zuordnen',
	'annuaire_editer' => 'Verzeichnis modifizieren',
	'annuaire_logo' => 'Logo des Verzeichnis',
	'annuaire_supprimer' => 'Verzeichnis löschen',
	'annuaire_un' => 'Ein Verzeichnis',
	'annuaires' => 'Verzeichnis',
	'annuaires_nb' => '@nb@ Verzeichnisse',
	'annuaires_tout' => 'Alle Verzeichnisse',
	'associe_a' => 'zugeordnet zu :',
	'associer_a' => 'Zuordnen zu :',
	'aucun_contact' => 'Kein Kontakt !',
	'aucune_organisation' => 'Keine Organisation !',
	'auteur_associe_est_a_la_poubelle' => 'Der Autor ist dem Papierkorb zugeordnet ! Dieser Autor wird in ein paar Tagen gelöscht.',
	'auteur_associe_inexistant' => 'Der zugeordnete Autor existiert nicht! Der Autor wurde vermutlich dem Papierkorb zugeordnet und gelöscht.',
	'auteur_lie' => 'ID des zugeordneten Autors',

	// B
	'bouton_contacts' => 'Kontakte',
	'bouton_contacts_organisations' => 'Kontakte & Organisationen',
	'bouton_organisations' => 'Organisationen',
	'bouton_rechercher' => 'Suchen',
	'bouton_repertoire' => 'Verzeichnis',

	// C
	'cfg_activer_squelettes_publics_zpip1' => 'Öffentliche Skelette für ZPIP v1',
	'cfg_activer_squelettes_publics_zpip1_explication' => 'Öffentliche Skelette für ZPIP Version 1 aktivieren, so dass Kontakte und Organisationen im öffentlichen Bereich der Website angezeigt werden können ?',
	'cfg_afficher_infos_sur_auteurs' => 'Details zu Autoren anzeigen ?',
	'cfg_afficher_infos_sur_auteurs_explication' => 'Kontakt- und Organisationsinformationen auch auf den Autorenseiten im Redaktionssystem anzeigen ?',
	'cfg_associer_aux_auteurs' => 'Autoren zuordnen ?',
	'cfg_associer_aux_auteurs_explication' => 'Den Autoren Kontakte und Organisationen zuordnen. Es wird Organisations- und Kontaktseiten ein Formular hinzugefügt, mit dem Autoren zugeordnet werden können. Die Autorenseiten werden entsprechend um ein Formular für die Zuordnung eines Kontakts oder einer Organisation ergänzt.',
	'cfg_lier_contacts_objets_explication' => 'Das Zuordnen von Kontakten zu den folgenden Inhalten erlauben. Damit wird die Kontaktauswahl auf den Seiten zur Verwaltung von Inhalten aktiviert.',
	'cfg_lier_contacts_objets_label' => 'Kontakte zuordnen',
	'cfg_lier_organisations_objets_explication' => 'Organisationen können den folgenden Inhalten zugeordnet werden. Damit wird die Auswahl von Organisationen auf den Seiten zur Verwaltung von Inhalten aktiviert.',
	'cfg_lier_organisations_objets_label' => 'Organisationen zuordnen',
	'cfg_relations_avec_auteurs' => 'Beziehung zu den Autoren',
	'cfg_relations_avec_objets' => 'Beziehung zu den Inhalten (im Unterschied zu den Autoren)',
	'cfg_supprimer_reciproquement_auteurs_et_contacts' => 'Verknüpfte Autoren und Kontakte löschen ?',
	'cfg_supprimer_reciproquement_auteurs_et_contacts_explication' => 'Wenn diese Option aktiv ist, wird beim Löschen eines Kontakts oder einer Organisation ein damit verbundener Autor in den Papierkorb verschoben. Umgekehrt wird beim Löschen eines Autors ein ihm zugeordneter Kontakt in den Papierkorb befördert. Diese Option verhindert verwaiste Kontakte, das Löschen kann dabei nicht rückgängig gemacht werden, sogar wenn der Autor Artikel geschrieben hat  …',
	'cfg_utiliser_annuaires_explication' => 'Ihre Kontakte und Organisationen können in mehrere Verzeichnisse aufgenommen werden.',
	'cfg_utiliser_annuaires_label' => 'Mehrere Verzeichnisse verwenden',
	'cfg_utiliser_organisations_arborescentes_explication' => 'Eine Organisation kann von einer anderen abstammen. Wenn diese Option aktiviert ist, wird ein Feld aktiviert, in dem diese Beziehung bezeichnet wird.',
	'cfg_utiliser_organisations_arborescentes_label' => 'Organisationsbaum verwenden',
	'changer' => 'Ändern',
	'chercher_contact' => 'Suchen',
	'chercher_organisation' => 'Suchen',
	'chercher_statut' => 'Status',
	'confirmer_delier_contact' => 'Wollen Sie diese Organisation wirklich von diesem Kontakt trennen ?',
	'confirmer_delier_organisation' => 'Wollen Sie diesen Kontakt wirklich von dieser Organisation trennen ??',
	'confirmer_delier_organisation_rubrique' => 'Wollen Sie diese Organisation wirklich von dieser Rubrik trennen ?',
	'confirmer_supprimer_contact' => 'Wollen Sie ie Informationen zu diesem Kontakt wirklich  löschen ?',
	'confirmer_supprimer_organisation' => 'Wollen Sie die Informationen zu dieser Organisation wirklich löschen ?',
	'contact' => 'Kontakt',
	'contact_ajouter' => 'Kontakt hinzufügen',
	'contact_ajouter_associe_a' => 'Kontakt hinzufügen und zurodnen :',
	'contact_ajouter_lien' => 'Diesen Kontakt hinzufügen',
	'contact_associe_a_auteur_numero' => 'Zuordnen zu Autor Nummer',
	'contact_associer_a_auteur' => 'Einem Autor zuordnen',
	'contact_aucun' => 'Kein Kontakt',
	'contact_creer' => 'Kontakt anlegen',
	'contact_creer_associer' => 'Kontakt anlegen und zuordnen',
	'contact_creer_dans_cet_annuaire' => 'Kontakt in diesem Verzeichnis anlegen',
	'contact_editer' => 'Kontakt bearbeiten',
	'contact_logo' => 'Kontaktlogo',
	'contact_nouveau_titre' => 'Neuer Kontakt',
	'contact_numero' => 'Kontakt Nummer',
	'contact_retirer_lien' => 'Kontakt entfernen',
	'contact_retirer_tous_lien' => 'Alle Kontakte entfernen',
	'contact_un' => 'Ein Kontakt',
	'contact_voir' => 'Ansehen',
	'contacts' => 'Kontakte',
	'contacts_filiales' => 'abhängige Kontakte',
	'contacts_nb' => '@nb@ Kontakte',
	'contacts_tout' => 'Alle Kontakte',
	'creer_auteur_contact' => 'Neuen Autor anlegen und diesem Objekt zuordnen',
	'creer_auteur_organisation' => 'Neuen Autor anlegen und dieser Organisation zuordnen',

	// D
	'definir_auteur_comme_contact' => 'Als Kontakt definieren',
	'definir_auteur_comme_organisation' => 'Als Organisation definieren',
	'delier_cet_auteur' => 'Zuordnung aufheben',
	'delier_contact' => 'Zuordnung aufheben',
	'delier_organisation' => 'Zuordnung aufheben',

	// E
	'erreur_annuaire_identifiant_existant' => 'Diese Bezeichnung wird bereits in einem Ihrer Verzeichnisse verwendet.',
	'est_un_contact' => 'Dieser Autor ist als Kontakt definiert.',
	'est_une_organisation' => 'Dieser Autor ist als Organisation definiert.',
	'explication_activite' => 'Tätigkeitsbereich der Organisation : Humanitär, Ausbildung, Verlag ...',
	'explication_contacts_ou_organisations' => 'Sie können diesen Autor als Kontakt oder Organisation definieren.',
	'explication_identification' => 'Identifikationscode der Organisation wie Steuer- oder Betriebsnummer.',
	'explication_statut_juridique' => 'GmbH, UG, AG, e.V. ...',

	// I
	'info_1_contact' => 'Ein Kontakt',
	'info_contacts_organisation' => 'Kontakte der Organisation',
	'info_nb_contacts' => 'zugeordnete Kontakte',
	'infos_contacts_ou_organisations' => 'Kontakte & Organisationen',

	// L
	'label_civilite' => 'Anrede',

	// S
	'statut_juridique' => 'Rechtsform',
	'suppression_automatique_de_organisation_prochainement' => 'Wenn Sie nichts weiter tun, wird Kontakte & Organisationen diese Organisation in den kommenden Tagen löschen.',
	'suppression_automatique_du_contact_prochainement' => 'Wenn Sie nichts weiter tun, wird Kontakte & Organisationen diesen Kontaktin den kommenden Tagen löschen.',
	'supprimer_contact' => 'Diesen Kontakt löschen',
	'supprimer_organisation' => 'Diese Organisation löschen',

	// T
	'titre_contact' => 'Kontaktdetails',
	'titre_contacts' => 'Die Kontakte',
	'titre_organisation' => 'Détails der Organisation',
	'titre_organisations' => 'Die Organisationen',
	'titre_page_configurer_contacts_et_organisations' => 'Kontakte & Organisationen konfigurieren',
	'titre_page_contacts' => 'Verwaltung von Kontakten',
	'titre_page_organisations' => 'Verwaltung von Organisationen',
	'titre_page_repertoire' => 'Verzeichnis',
	'titre_parametrages' => 'Einstellungen',
	'tous' => 'Alle'
);
